// YOUR CODE HERE
require('bootstrap/dist/css/bootstrap.css');

var angular = require('angular');
var module = angular.module('matchesModule', [])



module.controller('matchesController', ['$scope','$http', function($scope, $http) {
	$scope.filters = {};
	$scope.filters.gender = "";
	$scope.filters.ageMin = 18;
	$scope.filters.ageMax = 90;
	$scope.contactCardOpen = false;

	$scope.showContactInformation = function(match) {
		$scope.selectedMatch = match;
		$scope.contactCardOpen = true;
	}
	$scope.filterMatches = function(match) {
		if ((match.gender === $scope.filters.gender || $scope.filters.gender === '') && (match.age >= $scope.filters.ageMin && match.age <= $scope.filters.ageMax))
		{
			return true
		} else {
			return false;
		}

	}
	$scope.closeContact = function() {
		$scope.contactCardOpen = false;
	}

	$scope.reset = function() {
		$scope.filters.gender = "";
		$scope.filters.ageMin = 18;
		$scope.filters.ageMax = 90;
	}

	$http({
		method: "GET",
		url: "https://randomuser.me/api/?inc=gender,name,picture,dob,phone,cell,email&results=10"

	}).then(function successCallback(response) {
		var results = response.data.results;

		for (var i in results) {
			// Capitalize names
			results[i].name.first = (results[i].name.first).charAt(0).toUpperCase() + results[i].name.first.slice(1);
			results[i].name.last = (results[i].name.last).charAt(0).toUpperCase() + results[i].name.last.slice(1);
			// Convert date of birth to age
			results[i].age = new Date().getYear() - new Date(results[i].dob).getYear();
		}

		$scope.matches = results;

	});

}])


module.directive('match', function() {
	return {
		templateUrl: "matchTemplate.html"
	};
});

